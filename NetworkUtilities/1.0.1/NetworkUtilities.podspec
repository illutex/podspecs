#
# Be sure to run `pod lib lint NetworkUtilities.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NetworkUtilities'
  s.version          = '1.0.1'
  s.summary          = 'Network Utilities.'

  s.description      = <<-DESC
POD is providing Download Manager, Reachability Manager, Block Operation Queue & Data Requests
                       DESC

  s.homepage         = 'https://bitbucket.org/illutex/networkutilities.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Mike Ponomaryov' => 'dev.mikesoft@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/illutex/networkutilities.git', :tag => s.version.to_s }

  s.requires_arc            = true
  s.public_header_files     = 'Pod/Classes/**/*.h'

  #iOS
  s.ios.deployment_target   = '9.0'
  s.ios.source_files        = 'NetworkUtilities/Classes/**/*'

  #OSX
  s.osx.deployment_target   = '10.11'
  s.osx.source_files        = 'NetworkUtilities/Classes/**/*'


  s.source_files = 'NetworkUtilities/Classes/**/*'

  # s.frameworks = 'UIKit', 'AppKit'

end
