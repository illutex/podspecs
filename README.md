# TouchCast Podspecs

Collection of the private pods

## Installation

In order to be able to add libraries from the repository insert following line in the beginning of your Podfile

```ruby
source 'https://bitbucket.org/illutex/podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

## Author

Mike Ponomaryov, dev.mikesoft@gmail.com
